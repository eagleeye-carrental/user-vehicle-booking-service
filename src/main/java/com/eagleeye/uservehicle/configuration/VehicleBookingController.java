package com.eagleeye.uservehicle.configuration;

import commonentity.responseDto.RestResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/booking/order")
public class VehicleBookingController {
    @PostMapping("/create")
    ResponseEntity<RestResponse> bookVehicle(@RequestParam("vehicleId") String vehicleId,
                                             @RequestParam("userId") long userId,
                                             @RequestParam("bookingDays") LocalDate bookingDate) {
// todo:: change "booking date" name to appropriate naming convention
        //todo:: register process
        // update vehicle status in vehicle service
        return null;
    }

    @PostMapping("/cancel")
    ResponseEntity<RestResponse> cancelBooking(@RequestParam("userId") long userId,
                                               @RequestParam("vehicleId") String vehicleId) {
        return null;
        // todo:: cancel booking by user
        // update vehicle status in vehicle service
    }

    @PostMapping("/extend")
    ResponseEntity<RestResponse> extendBooking(@RequestParam("userId") long userId,
                                               @RequestParam("vehicleId") String vehicleId,
                                               @RequestParam("extendedDate") LocalDate extendedDate) {
        //todo:: extend booking date process
        return null;
    }

    @GetMapping("/byUser")
    ResponseEntity<RestResponse> userBookOrder(@RequestParam("userId") long userId) {
        // todo:: find user booking orders process
        // return one to more vehicle order by user
        return null;
    }

    @GetMapping("/byVehicle")
    ResponseEntity<RestResponse> vehicleBookOrder(@RequestParam("userId") long userId) {
        // todo:: find user booking orders process
        // return how many times this vehicle owned by which user
        return null;
    }


}
